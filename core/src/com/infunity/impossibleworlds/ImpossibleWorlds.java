package com.infunity.impossibleworlds;

import aurelienribon.tweenengine.Tween;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.infunity.impossibleworlds.Screens.GameScreen;
import com.infunity.impossibleworlds.TweenAccessors.ColorTween;
import com.infunity.impossibleworlds.TweenAccessors.SpriteTween;

public class ImpossibleWorlds extends Game {

    public static final float TARGET_WIDTH = 1024;
    public static final float TARGET_HEIGHT = 576;

    public static final boolean DEBUG = false;
    FPSLogger log;

    @Override
	public void create () {
        log = new FPSLogger();

        Tween.registerAccessor(Color.class, new ColorTween());
        Tween.registerAccessor(Sprite.class, new SpriteTween());
        Tween.setCombinedAttributesLimit(4);

        this.setScreen(new GameScreen(this, 1, null));
	}

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void render() {
        super.render();
        log.log();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }
}
