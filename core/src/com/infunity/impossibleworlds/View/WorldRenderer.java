package com.infunity.impossibleworlds.View;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.infunity.impossibleworlds.ImpossibleWorlds;
import com.infunity.impossibleworlds.Model.GameWorld;
import com.infunity.impossibleworlds.Utils.ColorUtils;

public class WorldRenderer {

	private GameWorld world;
    public static OrthographicCamera cam;
	public static Rectangle viewport;
	private SpriteBatch batch;

    private Sprite bg0;
    private Sprite bg1;

    public static float STILL_CAM_TIME = 0;

	public WorldRenderer(GameWorld world) {
		this.world = world;

		cam = new OrthographicCamera();
		cam.setToOrtho(false, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());

		batch = new SpriteBatch();

        bg0 = new Sprite(new Texture(Gdx.files.internal("graphics/city_bg_0.png")));
        bg0.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bg0.getTexture().setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        bg0.setPosition(0, 0);
        bg1 = new Sprite(new Texture(Gdx.files.internal("graphics/city_bg_1.png")));
        bg1.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bg1.getTexture().setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        bg1.setPosition(0, 0);


	}

    float lastCamYPos = 0;

	public void render(float delta) {
        if(STILL_CAM_TIME > 0) {
            STILL_CAM_TIME-= delta;
        } else {
            cam.position.x =  MathUtils.lerp(cam.position.x, world.getMap().getPlayer().getPositionX(), delta * 5);
        }

        // Limit cam movement from left
        if(cam.position.x - cam.viewportWidth / 2 < 0) {
            cam.position.x = 0 + cam.viewportWidth / 2;
        }

        // Limit cam movement from right
        if(cam.position.x + cam.viewportWidth / 2 > world.getMap().getWidth()) {
            cam.position.x = world.getMap().getWidth() - cam.viewportWidth / 2;
        }

        cam.position.y = viewport.height/2;
        cam.update();
        batch.setProjectionMatrix(cam.combined);

        world.getMap().getTiledMapRenderer().setView(cam);

        bg0.setPosition(cam.position.x - cam.viewportWidth / 2, 0);
        bg1.setPosition(cam.position.x - cam.viewportWidth / 2, 0);
        bg0.scroll((cam.position.x - lastCamYPos) / 60 * delta, 0);
        bg1.scroll((cam.position.x - lastCamYPos) / 100 * delta, 0);
        lastCamYPos = cam.position.x;


        Color clearColor = world.getMap().getBackgroundColor();
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();

        bg1.setColor(world.getMap().get100Color());
        bg0.setColor(world.getMap().getLightColor());
        bg1.draw(batch);
        bg0.draw(batch);

		world.getMap().draw(batch);
		batch.end();
		
		if (ImpossibleWorlds.DEBUG) {
			world.getBox2dworld().render(cam);
		}
	}

	public void resize(int width, int height) {
        float scale = (float)height/(float)world.getGame().TARGET_HEIGHT;
        float newWidth = (float)world.getGame().TARGET_WIDTH * scale;
        float diff = width - newWidth;

        viewport = new Rectangle(0, 0, (float)world.getGame().TARGET_WIDTH + (diff/scale), (float)world.getGame().TARGET_HEIGHT);

        //System.out.println(viewport);

        cam.position.x = viewport.width/2 /*+ (diff/scale)*/ /*+ 600*/;
        cam.position.y = viewport.height/2;
        cam.viewportHeight = viewport.height;
        cam.viewportWidth = viewport.width;

        cam.update();
        batch.setProjectionMatrix(cam.combined);

        world.getMap().getTiledMapRenderer().setView(cam);
	}
	
	public void resume() {
	}

	public OrthographicCamera getCamera() {
		return cam;
	}
	
	public void dispose() {
		batch.dispose();
	}


}
