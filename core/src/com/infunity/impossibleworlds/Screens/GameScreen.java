package com.infunity.impossibleworlds.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.infunity.impossibleworlds.ImpossibleWorlds;
import com.infunity.impossibleworlds.Model.Box2DWorld;
import com.infunity.impossibleworlds.Model.GameWorld;
import com.infunity.impossibleworlds.Model.LayerEnum;
import com.infunity.impossibleworlds.Model.Maps.*;
import com.infunity.impossibleworlds.View.WorldRenderer;

/**
 * Created by Lukasz on 2014-08-23.
 */
public class GameScreen implements Screen {

    private GameWorld world; // Logic
    private WorldRenderer render; // Renderer
    private Box2DWorld box2dworld;
    private Map map;

    /*
     * Things for Fixed Timestep - look into render for implementation and docs
     */
    private float fixedTimestepAccumulator = 0f;
    private final float MAX_ACCUMULATED_TIME = 1.0f;
    private final float TIMESTEP = 1/60f;

    public GameScreen(ImpossibleWorlds game, int mapNum, LayerEnum layer) {
        box2dworld = new Box2DWorld(new Vector2(0, -25));

        // Ugly!
        switch(mapNum) {
            case 1:
                map = new Map1(box2dworld);
                break;
            case 2:
                map = new Map2(box2dworld);
                break;
            case 3:
                map = new Map3(box2dworld);
                break;
            case 4:
                map = new Map4(box2dworld);
                break;
            case 5:
                map = new Map5(box2dworld);
                break;
            case 6:
                map = new Map6(box2dworld);
                break;
            case 7:
                map = new Map7(box2dworld);
                break;
            default:
                Gdx.app.exit();
                break;
        }

        if(layer != null) {
            map.tweenFrom(layer);
        }

        world = new GameWorld(game, box2dworld, map, mapNum);
        render = new WorldRenderer(world);


//        Gdx.input.setInputProcessor(new InputController(map, game));

    }

    @Override
    public void render(float delta) {
		/*
		 * Implementation of fixed timestep
		 * docs:
		 * - http://gafferongames.com/game-physics/fix-your-timestep/
		 * - http://temporal.pr0.pl/devblog/download/arts/fixed_step/fixed_step.pdf
		 */

        fixedTimestepAccumulator += delta;
        if(fixedTimestepAccumulator > MAX_ACCUMULATED_TIME)
            fixedTimestepAccumulator = MAX_ACCUMULATED_TIME;

        while (fixedTimestepAccumulator >= TIMESTEP) {

			/*
			 * Update physics
			 */
            box2dworld.update(TIMESTEP);
            world.update(TIMESTEP);
            fixedTimestepAccumulator -= TIMESTEP;
        }

		/*
		 * Render
		 */
        render.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        render.resize(width, height);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {
        dispose();

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        map.dispose();
    }

}
