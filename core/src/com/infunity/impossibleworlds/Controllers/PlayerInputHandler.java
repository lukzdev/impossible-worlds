package com.infunity.impossibleworlds.Controllers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.infunity.impossibleworlds.Model.Maps.Map;

/**
 * Created by Lukasz on 2014-08-23.
 */
public class PlayerInputHandler implements InputProcessor {

    private Map map;

    public PlayerInputHandler(Map map) {
        this.map = map;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch(keycode) {
            case Input.Keys.UP:
                map.getPlayer().setJumpButton(true);
                break;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode) {
            case Input.Keys.UP:
                map.getPlayer().setJumpButton(false);
                break;
            case Input.Keys.R:
                map.setMapRestart(true);
                break;
            case Input.Keys.SPACE:
                if(!map.isLayerTweening())
                    map.switchLayer();
                break;
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
