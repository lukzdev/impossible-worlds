package com.infunity.impossibleworlds.Utils;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.infunity.impossibleworlds.Model.Box2DWorld;
import com.infunity.impossibleworlds.Model.Entities.Ground;
import com.infunity.impossibleworlds.Model.Entities.Portal;
import com.infunity.impossibleworlds.Model.Entities.Spikes;
import com.infunity.impossibleworlds.Model.Maps.Map;

public class TiledMapProcessor {

    public static void createGroundObjects(Map map, MapLayer layer, Box2DWorld world) {
        for(MapObject object : layer.getObjects()) {

            if(!(object instanceof PolylineMapObject))
                continue;

            PolylineMapObject polyObject = (PolylineMapObject)object;
            Polyline polyline = new Polyline(polyObject.getPolyline().getTransformedVertices());
            polyline.setScale(world.WORLD_TO_BOX, world.WORLD_TO_BOX);

            Body groundBody = world.getBodyBuilder()
                    .fixture(world.getFixtureDefBuilder()
                            .chainShape(polyline.getTransformedVertices()))
                            //.position(RoboRunner.TARGET_WIDTH / 2, 5)
                    .type(BodyDef.BodyType.StaticBody)
                    .userData(map)
                    .build();

            map.getEntMan().addEntity(new Ground(polyline.getX() * polyline.getScaleX(),
                    polyline.getY() * polyline.getScaleY(), polyline.getLength(), 0, groundBody));

        }
    }

    public static void createSpikeObjects(Map map, MapLayer layer, Box2DWorld world) {
        for(MapObject object : layer.getObjects()) {

            if(!(object instanceof PolylineMapObject))
                continue;

            PolylineMapObject polyObject = (PolylineMapObject)object;
            Polyline polyline = new Polyline(polyObject.getPolyline().getTransformedVertices());
            polyline.setScale(world.WORLD_TO_BOX, world.WORLD_TO_BOX);

            Body groundBody = world.getBodyBuilder()
                    .fixture(world.getFixtureDefBuilder()
                            .chainShape(polyline.getTransformedVertices()))
                            //.position(RoboRunner.TARGET_WIDTH / 2, 5)
                    .type(BodyDef.BodyType.StaticBody)
                    .userData(map)
                    .build();

            map.getEntMan().addEntity(new Spikes(polyline.getX() * polyline.getScaleX(),
                    polyline.getY() * polyline.getScaleY(), polyline.getLength(), 0, groundBody));

        }
    }

    public static Vector2 getStartPos(TiledMap map) {
        Object startX = map.getProperties().get("playerX");
        Object startY = map.getProperties().get("playerY");
        Vector2 startPos = new Vector2(0, 0);

        if(startX != null && startY != null) {
            startPos.set(Float.valueOf((String)startX), Float.valueOf((String)startY));
        }
        startPos.y = (Integer)map.getProperties().get("height") - startPos.y;

        startPos.x *= (Integer)map.getProperties().get("tilewidth");
        startPos.y *= (Integer)map.getProperties().get("tileheight");

        startPos.x += (Integer)map.getProperties().get("tilewidth") / 2;
        startPos.y -= (Integer)map.getProperties().get("tileheight") / 2;

        return startPos;
    }

    public static void createPortal(Map map, MapLayer layer, Box2DWorld world) {
        for(MapObject object : layer.getObjects()) {

            if(!(object instanceof RectangleMapObject))
                continue;

            RectangleMapObject rectObject = (RectangleMapObject)object;

            map.getEntMan().addEntity(new Portal(rectObject.getRectangle().getX(),
                    rectObject.getRectangle().getY(), 64, 102, map, world));

        }

    }

}
