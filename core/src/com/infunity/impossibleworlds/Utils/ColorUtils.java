package com.infunity.impossibleworlds.Utils;

import com.badlogic.gdx.graphics.Color;
import com.infunity.impossibleworlds.Model.LayerEnum;


public class ColorUtils {

    public static final Color DARK_RED = new Color(229.f/255.f,28.f/255.f,35.f/255.f, 1); // 500
    public static final Color LIGHT_RED = new Color(246.f/255.f, 153.f/255.f, 136.f/255.f, 1); // 100
    public static final Color RED_100 = Color.valueOf("f9bdbbff");
    public static final Color BACKGROUND_RED = Color.valueOf("fcd8d2");//new Color(253.f/255.f, 224.f/255.f, 220.f/255.f, 1); // 50
    public static final Color BUTTON_RED = Color.valueOf("c41411");

    public static final Color DARK_GREEN = new Color(37.f/255.f, 155.f/255.f, 36.f/255.f, 1);
    public static final Color LIGHT_GREEN = new Color(114.f/255.f,213.f/255.f,114.f/255.f, 1f);
    public static final Color GREEN_100 = Color.valueOf("a3e9a4ff");
    public static final Color BACKGROUND_GREEN = Color.valueOf("c7fdc4");//new Color(208.f/255.f, 248.f/255.f, 206.f/255.f, 1);
    public static final Color BUTTON_GREEN = Color.valueOf("056f00");

    public static final Color DARK_BLUE = new Color(86.f/255.f,119.f/255.f,252.f/255.f, 1);
    public static final Color LIGHT_BLUE = new Color(175.f/255.f, 191.f/255.f, 255.f/255.f, 1);
    public static final Color BLUE_100 = Color.valueOf("d0d9ffff");
    public static final Color BACKGROUND_BLUE = Color.valueOf("dee1fd");//new Color(231.f/255.f, 233.f/255.f, 253.f/255.f, 1);
    public static final Color BUTTON_BLUE = Color.valueOf("3b50ce");

    public static Color layerToBackgroundColor(LayerEnum layer) {
        if (layer == LayerEnum.R) {
            return ColorUtils.BACKGROUND_RED;
        } else if (layer == LayerEnum.G) {
            return ColorUtils.BACKGROUND_GREEN;
        } else if (layer == LayerEnum.B) {
            return ColorUtils.BACKGROUND_BLUE;
        }
        return Color.WHITE;
    }

    public static Color layerTo100Color(LayerEnum layer) {
        if (layer == LayerEnum.R) {
            return ColorUtils.RED_100;
        } else if (layer == LayerEnum.G) {
            return ColorUtils.GREEN_100;
        } else if (layer == LayerEnum.B) {
            return ColorUtils.BLUE_100;
        }
        return Color.WHITE;
    }

    public static Color layerToLightColor(LayerEnum layer) {
        if (layer == LayerEnum.R) {
            return ColorUtils.LIGHT_RED;
        } else if (layer == LayerEnum.G) {
            return ColorUtils.LIGHT_GREEN;
        } else if (layer == LayerEnum.B) {
            return ColorUtils.LIGHT_BLUE;
        }
        return Color.WHITE;
    }

    public static Color layerToDarkColor(LayerEnum layer) {
        if (layer == LayerEnum.R) {
            return ColorUtils.DARK_RED;
        } else if (layer == LayerEnum.G) {
            return ColorUtils.DARK_GREEN;
        } else if (layer == LayerEnum.B) {
            return ColorUtils.DARK_BLUE;
        }
        return Color.WHITE;
    }

    public static Color layerToButtonColor(LayerEnum layer) {
        if (layer == LayerEnum.R) {
            return ColorUtils.BUTTON_RED;
        } else if (layer == LayerEnum.G) {
            return ColorUtils.BUTTON_GREEN;
        } else if (layer == LayerEnum.B) {
            return ColorUtils.BUTTON_BLUE;
        }
        return Color.WHITE;
    }

}
