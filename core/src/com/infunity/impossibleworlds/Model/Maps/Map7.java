package com.infunity.impossibleworlds.Model.Maps;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.infunity.impossibleworlds.Model.Box2DWorld;


public class Map7 extends Map {



	public Map7(Box2DWorld box2dworld) {
		super(0, 0, box2dworld);

        TmxMapLoader.Parameters params = new TmxMapLoader.Parameters();
        params.textureMagFilter = Texture.TextureFilter.Linear;
        params.textureMinFilter = Texture.TextureFilter.Linear;
        tiledMap = new TmxMapLoader().load("maps/map7.tmx", params);

        Integer widthStr = (Integer)tiledMap.getProperties().get("width");
        Integer tileWidthStr = (Integer)tiledMap.getProperties().get("tilewidth");
        this.width = widthStr * tileWidthStr;

        processMap();
	}
	
	@Override
	public void dispose() {
        player.dispose();
        entMan.dispose();
	}

    @Override
    public void resetGame() {
//        super.resetGame();
    }

    @Override
	public void update(float delta) {
		super.update(delta);
	}
	
	@Override
	public void draw(SpriteBatch batch) {
        super.draw(batch);
	}

}
