package com.infunity.impossibleworlds.Model.Maps;

import aurelienribon.tweenengine.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.infunity.impossibleworlds.ImpossibleWorlds;
import com.infunity.impossibleworlds.Model.Box2DWorld;
import com.infunity.impossibleworlds.Model.Entities.GhostPlayer;
import com.infunity.impossibleworlds.Model.Entities.Player;
import com.infunity.impossibleworlds.Model.EntityManager;
import com.infunity.impossibleworlds.Model.LayerEnum;
import com.infunity.impossibleworlds.Model.PhysicsObject;
import com.infunity.impossibleworlds.TweenAccessors.ColorTween;
import com.infunity.impossibleworlds.TweenAccessors.SpriteTween;
import com.infunity.impossibleworlds.Utils.ColorUtils;
import com.infunity.impossibleworlds.Utils.TiledMapProcessor;
import com.infunity.impossibleworlds.View.WorldRenderer;


public abstract class Map implements ContactListener  {

	protected float width;
	protected float height;
	protected Box2DWorld box2dworld;
	protected Player player;

    protected EntityManager entMan;

    protected LayerEnum currentLayer;
    protected LayerEnum nextLayer;

    protected TiledMap tiledMap;
    protected OrthogonalTiledMapRenderer tiledMapRenderer;

    protected boolean mapFinished = false;
    protected boolean mapRestart = false;

    // Color tweening!
    protected Color backgroundColor;
    protected Color darkColor;
    protected Color a100Color;
    protected Color lightColor;
    protected boolean layerTweening = false;

    protected TweenManager tweenMgr;
    protected ParticleEffect explosion;

    private Sound layerSound;

    protected Sprite tweenLevelSprite;
    protected boolean nextLevelTween = false;
    protected boolean prevLevelTween = false;

	public Map(float width, float height, Box2DWorld box2dworld) {
		this.width = width;
		this.height = height;
		this.box2dworld = box2dworld;

        entMan = new EntityManager(this);
        box2dworld.getWorld().setContactListener(this);

		this.player = new Player(200, 500, 64, box2dworld);

        entMan.addGhost(new GhostPlayer(0, 0, 64, 64, box2dworld, LayerEnum.R));
        entMan.addGhost(new GhostPlayer(0, 0, 64, 64, box2dworld, LayerEnum.G));
        entMan.addGhost(new GhostPlayer(0, 0, 64, 64, box2dworld, LayerEnum.B));

        tweenMgr = new TweenManager();

        // Set initial colors
        nextLayer();
        player.setColor(ColorUtils.layerToDarkColor(currentLayer));
        backgroundColor = new Color(ColorUtils.layerToBackgroundColor(currentLayer));
        darkColor = new Color(ColorUtils.layerToDarkColor(currentLayer));
        a100Color = new Color(ColorUtils.layerTo100Color(currentLayer));
        lightColor = new Color(ColorUtils.layerToLightColor(currentLayer));

        layerSound = Gdx.audio.newSound(Gdx.files.internal("sounds/layer.wav"));

        explosion = new ParticleEffect();
        explosion.load(Gdx.files.internal("particles/explosion.p"), Gdx.files.internal("graphics"));

        tweenLevelSprite = new Sprite(new Texture(Gdx.files.internal("graphics/square-tile.png")));
        tweenLevelSprite.setAlpha(0);
        tweenLevelSprite.setPosition(0, 0);
	}

    public void tweenFrom(LayerEnum layer) {
        tweenLevelSprite.setAlpha(1);
        tweenLevelSprite.setPosition(0, 0);
        tweenLevelSprite.setSize(ImpossibleWorlds.TARGET_WIDTH + 100, ImpossibleWorlds.TARGET_HEIGHT);

//        tweenLevelSprite.setColor(ColorUtils.layerToDarkColor(layer));
        tweenLevelSprite.setColor(ColorUtils.layerToDarkColor(currentLayer));
        player.setRestartTime(0.5f);

        prevLevelTween = true;

        Timeline.createSequence()
//                .push(Tween.to(tweenLevelSprite, SpriteTween.COLOR, 0.2f).target(darkColor.r, darkColor.g, darkColor.b, darkColor.a))
                .push(Tween.to(tweenLevelSprite, SpriteTween.ALPHA, 0.5f).target(0f))
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        prevLevelTween = false;
                    }
                })
                .start(tweenMgr);
    }

    public void processMap() {
        TiledMapProcessor.createGroundObjects(this, tiledMap.getLayers().get("ground_objects"), box2dworld);
        TiledMapProcessor.createPortal(this, tiledMap.getLayers().get("ground_objects"), box2dworld);
        TiledMapProcessor.createSpikeObjects(this, tiledMap.getLayers().get("spike_objects"), box2dworld);

        // Process player position
        Vector2 playerPos = TiledMapProcessor.getStartPos(tiledMap);
        playerPos.y += player.getHeight() / 3;
        player.setStartPos(playerPos);
        player.setBodyPosition(playerPos);

        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
    }

    public void nextLvl() {

    }
	
	public void update(float delta) {
        entMan.update(delta);

        if(!nextLevelTween)
            player.update(delta);

        tweenMgr.update(delta);
        explosion.update(delta);
    }

	public void draw(SpriteBatch batch) {

        batch.end();
        tiledMapRenderer.getSpriteBatch().setColor(darkColor);
        tiledMapRenderer.render();
        batch.begin();

        entMan.draw(batch);

        //explosion.draw(batch);

		player.draw(batch);

        if(nextLevelTween || prevLevelTween)
            tweenLevelSprite.draw(batch);
	}

    public void explode(float x, float y) {
        explosion.getEmitters().get(0).getTint().setColors(new float[] {darkColor.r, darkColor.g, darkColor.b});
        explosion.setPosition(x, y);
        explosion.start();
    }

    public void switchLayer() {
        layerTweening = true;
        layerSound.play();
        // Spawn ghost in current pos
        GhostPlayer ghost = entMan.getGhost(currentLayer);
        ghost.activate(player.getPositionX(), player.getPositionY());

        player.restartPos(false);
        nextLayer();

        Color targetBgColor = ColorUtils.layerToBackgroundColor(currentLayer);
        Color targetDarkColor = ColorUtils.layerToDarkColor(currentLayer);
        Color target100Color = ColorUtils.layerTo100Color(currentLayer);
        Color targetLightColor = ColorUtils.layerToLightColor(currentLayer);

        // Update colorst
        Timeline.createSequence()
                .beginParallel()
                    .push(Tween.to(backgroundColor, ColorTween.COLOR, 0.5f).target(targetBgColor.r, targetBgColor.g, targetBgColor.b, targetBgColor.a))
                    .push(Tween.to(darkColor, ColorTween.COLOR, 0.5f).target(targetDarkColor.r, targetDarkColor.g, targetDarkColor.b, targetDarkColor.a))
                    .push(Tween.to(a100Color, ColorTween.COLOR, 0.5f).target(target100Color.r, target100Color.g, target100Color.b, target100Color.a))
                    .push(Tween.to(lightColor, ColorTween.COLOR, 0.5f).target(targetLightColor.r, targetLightColor.g, targetLightColor.b, targetLightColor.a))
                .end()
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        layerTweening = false;
                    }
                })
                .start(tweenMgr);

        player.setColor(ColorUtils.layerToDarkColor(currentLayer));
        player.getSprite().setAlpha(0);
        Tween.to(player.getSprite(), SpriteTween.ALPHA, 0.5f)
                .target(1f)
                .start(tweenMgr);

        // Deactivate ghost of new layer
        final GhostPlayer ghost2 = entMan.getGhost(currentLayer);
        Tween.to(ghost2.getSprite(), SpriteTween.ALPHA, 0.5f)
                .target(0f)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        ghost2.deactivate();
                    }
                })
                .start(tweenMgr);

        WorldRenderer.STILL_CAM_TIME = 0.2f;
    }

    public void tweenFinish() {
        nextLevelTween = true;
        tweenLevelSprite.setColor(darkColor);
        tweenLevelSprite.setSize(WorldRenderer.viewport.width, WorldRenderer.viewport.height);
        tweenLevelSprite.setAlpha(0);

        tweenLevelSprite.setPosition(WorldRenderer.cam.position.x - WorldRenderer.cam.viewportWidth /2,
                WorldRenderer.cam.position.y - WorldRenderer.cam.viewportHeight /2);

        Tween.to(tweenLevelSprite, SpriteTween.ALPHA, 0.5f)
                .target(1f)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        setMapFinished(true);
                    }
                })
                .start(tweenMgr);

    }
		
	public abstract void dispose();
	public abstract void resetGame();
	
	@Override
	public void beginContact(Contact contact) {
		// So beautiful! Weeeeeeeeeeeeeeeeeeeee ^.^
		Object ent1 = (Object)contact.getFixtureA().getBody().getUserData();
        Object ent2 = (Object)contact.getFixtureB().getBody().getUserData();

        if(!(ent1 instanceof PhysicsObject) || !(ent2 instanceof PhysicsObject)) {
            return;
        }

		PhysicsObject physo1 = (PhysicsObject)ent1;
		PhysicsObject physo2 = (PhysicsObject)ent2;

        Vector2[] contactPoints = contact.getWorldManifold().getPoints();

		physo1.handleBeginContact(physo2, contactPoints, this);
		physo2.handleBeginContact(physo1, contactPoints, this);
	}
	
	@Override
	public void endContact(Contact contact) {
        // So beautiful! Weeeeeeeeeeeeeeeeeeeee ^.^
        Object ent1 = (Object)contact.getFixtureA().getBody().getUserData();
        Object ent2 = (Object)contact.getFixtureB().getBody().getUserData();

        if(!(ent1 instanceof PhysicsObject) || !(ent2 instanceof PhysicsObject)) {
            return;
        }

        PhysicsObject physo1 = (PhysicsObject)ent1;
        PhysicsObject physo2 = (PhysicsObject)ent2;

        Vector2[] contactPoints = contact.getWorldManifold().getPoints();

        physo1.handleEndContact(physo2, contactPoints, this);
        physo2.handleEndContact(physo1, contactPoints, this);
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

    protected LayerEnum getNextLayer(LayerEnum currentLayer) {
        int layerNum = MathUtils.random(2);

//        int i = 0;
//        for(i = 0; i < LayerEnum.values().length; i++) {
//            if(LayerEnum.values()[i] == currentLayer) {
//                layerNum = i;
//            }
//        }
        if(currentLayer != null) {
            layerNum = currentLayer.ordinal();
        }

        layerNum++;
        layerNum %= 3;

        return LayerEnum.values()[layerNum];
    }

    public void nextLayer() {
        if(nextLayer == null) {
            nextLayer = getNextLayer(currentLayer);
        }

        currentLayer = nextLayer;
        nextLayer = getNextLayer(currentLayer);
    }

    public OrthogonalTiledMapRenderer getTiledMapRenderer() {
        return tiledMapRenderer;
    }

    public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}

    public EntityManager getEntMan() {
        return entMan;
    }

    public Box2DWorld getBox2dworld() {
		return box2dworld;
	}

	public void setBox2Dworld(Box2DWorld box2dworld) {
		this.box2dworld = box2dworld;
	}

	public Player getPlayer() {
		return player;
	}

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getLightColor() {
        return lightColor;
    }

    public Color get100Color() {
        return a100Color;
    }

    public Color getDarkColor() {
        return darkColor;
    }

    public boolean isLayerTweening() {
        return layerTweening;
    }

    public boolean isMapFinished() {
        return mapFinished;
    }

    public void setMapFinished(boolean mapFinished) {
        this.mapFinished = mapFinished;
    }

    public boolean isMapRestart() {
        return mapRestart;
    }

    public void setMapRestart(boolean mapRestart) {
        this.mapRestart = mapRestart;
    }

    public LayerEnum getCurrentLayer() {
		return currentLayer;
	}

}
