package com.infunity.impossibleworlds.Model;

import com.badlogic.gdx.Gdx;
import com.infunity.impossibleworlds.Controllers.PlayerInputHandler;
import com.infunity.impossibleworlds.ImpossibleWorlds;
import com.infunity.impossibleworlds.Model.Maps.Map;
import com.infunity.impossibleworlds.Screens.GameScreen;

public class GameWorld {
//	public static final int GAME_READY = 0;
//	public static final int GAME_RUNNING = 1;
//	public static final int GAME_PAUSED = 2;
//	public static final int GAME_OVER = 4;
//	public static final int GAME_FINISHED = 5;
//	public static final int GAME_RESUME = 6;
	
//	private static int state = GAME_RUNNING;

    private ImpossibleWorlds game;
	private Map map;
	private Box2DWorld box2dworld;

    private int mapNum;

	public GameWorld(ImpossibleWorlds game, Box2DWorld box2dword, Map map, int mapNum) {
		this.game = game;
		this.map = map;
		this.box2dworld = box2dword;
        this.mapNum = mapNum;

        box2dword.getWorld().setContactListener(map);
        Gdx.input.setInputProcessor(new PlayerInputHandler(map));
	}
	
	public void resetGame() {
		map.resetGame();
	}

	public void update(float delta) {
        if(map.isMapFinished()) {
            game.setScreen(new GameScreen(game, ++mapNum, map.getCurrentLayer()));
        }

        if(map.isMapRestart()) {
            game.setScreen(new GameScreen(game, mapNum, null));
        }

        map.update(delta);
	}


    public ImpossibleWorlds getGame() {
        return game;
    }

    public Box2DWorld getBox2dworld() {
		return box2dworld;
	}
	
	public Map getMap() {
		return map;
	}
	
	public void dispose() {
	}

}
