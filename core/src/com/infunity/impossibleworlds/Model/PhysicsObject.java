package com.infunity.impossibleworlds.Model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.infunity.impossibleworlds.Model.Maps.Map;

public interface PhysicsObject {
    public void handleBeginContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map);
    public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map);

    public Body getBody();
    public boolean getFlagForDelete();
    public void setFlagForDelete(boolean flag);
}
