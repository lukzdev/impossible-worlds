package com.infunity.impossibleworlds.Model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.infunity.impossibleworlds.Model.Entities.Entity;
import com.infunity.impossibleworlds.Model.Entities.GhostPlayer;
import com.infunity.impossibleworlds.Model.Maps.Map;

public class EntityManager {

    private Map map;
    private Array<Entity> entities;
    private Array<GhostPlayer> ghosts;

    public EntityManager(Map map) {
        this.map = map;
        this.entities = new Array<Entity>();

        this.ghosts = new Array<GhostPlayer>(3);
    }


    public void update(float delta) {
        for(Entity ent : entities) {
            ent.update(delta);
        }
    }

    public void draw(SpriteBatch batch) {
        for(Entity ent : entities) {
            ent.draw(batch);
        }

        for(GhostPlayer ghost : ghosts) {
            ghost.draw(batch);
        }
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
    }

    public void addGhost(GhostPlayer ghost) {
        ghosts.add(ghost);
    }

    public GhostPlayer getGhost(LayerEnum layer) {
        for(GhostPlayer ghost : ghosts) {
            if(ghost.getLayer() == layer) {
                return ghost;
            }
        }

        return null;
    }

    public void dispose() {
    }

}
