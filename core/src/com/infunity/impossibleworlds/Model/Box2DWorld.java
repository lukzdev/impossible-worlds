package com.infunity.impossibleworlds.Model;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.infunity.impossibleworlds.Utils.BodyBuilder;
import com.infunity.impossibleworlds.Utils.FixtureDefBuilder;
import com.infunity.impossibleworlds.Utils.JointBuilder;

/**
 * Created by Lukasz on 2014-08-23.
 */
public class Box2DWorld {

    /*
     * Statics for calculation pixel to box2d metrics and vice versa
     */
    public static final float WORLD_TO_BOX = 0.01f;
    public static final float BOX_TO_WORLD = 100f;

    // Used to sweep dead bodies, check below for more info
//    Array<Body> bodies;

    /*
     * Physics simulation world
     */
    World world;
    Box2DDebugRenderer renderer;

    private FixtureDefBuilder fixtureDefBuilder;
    private BodyBuilder bodyBuilder;
    private JointBuilder jointBuilder;

    public Box2DWorld(Vector2 vector2) {
        World.setVelocityThreshold(WORLD_TO_BOX);
        world = new World(vector2, false);
//        bodies = new Array<Body>();
        renderer = new Box2DDebugRenderer(true, true, false, true, true, true);

        bodyBuilder = new BodyBuilder(world);
        fixtureDefBuilder = new FixtureDefBuilder();
        jointBuilder = new JointBuilder(world);
    }

    public void update(float dt) {
        world.step(dt, 6, 2);
//        sweepDeadBodies();
    }


    /*
     * Bodies should be removed after world step to prevent simulation crash
     */
//    public void sweepDeadBodies() {
//        world.getBodies(bodies);
//
//        for(Body body : bodies) {
//            if (body != null && (body.getUserData() instanceof PhysicsObject)) {
//                PhysicsObject data = (PhysicsObject) body.getUserData();
//                if (data.getFlagForDelete()) {
//                    getWorld().destroyBody(body);
//                    body.setUserData(null);
//                    body = null;
//                }
//            }
//        }
//
////		for (Iterator<Body> iter = bodies.iterator(); iter.hasNext();) {
////			Body body = iter.next();
////			if (body != null && (body.getUserData() instanceof Player)) {
////				Player data = (Player) body.getUserData();
////				if (data.isFlaggedForDelete) {
////					getWorld().destroyBody(body);
////					body.setUserData(null);
////					body = null;
////				}
////			}
////		}
//    }

    /*
     * Render box2d debug
     */
    public void render(Camera cam) {
        renderer.render(world, cam.combined.cpy().scl(BOX_TO_WORLD));
    }

    /**
     * @return the world
     */
    public World getWorld() {
        return world;
    }

    public BodyBuilder getBodyBuilder() {
        return bodyBuilder;
    }

    public FixtureDefBuilder getFixtureDefBuilder() {
        return fixtureDefBuilder;
    }

    public JointBuilder getJointBuilder() {
        return jointBuilder;
    }

}
