package com.infunity.impossibleworlds.Model.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.infunity.impossibleworlds.Model.Box2DWorld;
import com.infunity.impossibleworlds.Model.Maps.Map;
import com.infunity.impossibleworlds.Model.PhysicsObject;

public class Player extends MoveableEntity implements PhysicsObject {

	private Body body;
    private PlayerOnGroundSensor onGroundSensor;
    private Sprite sprite;

    private boolean jumpButton = false;
    private boolean firstJumpOnButtonPress = false;
    private boolean isJumping = false;
    private boolean isGoingUp = false;

    // Jumping animation
    private boolean animationStarted = false;
    private boolean maxSizeX = false;
    private boolean maxSizeY = false;

    public int onGround = 0;

    private Vector2 startPos;
    private Vector2 lastVelocity;

    private boolean shouldRestart = false;

    private float restartTime = 0.05f;

    private Sound jumpSound;
    private Sound explosionSound;
    private Sound finishSound;

	public Player(float x, float y, float width, Box2DWorld box2dworld) {
		super(1f, 0, width, width, x, y);

        // Player body
		this.body = box2dworld.getBodyBuilder()
		        .fixture(box2dworld.getFixtureDefBuilder()
                        .boxShape(width / 2, width / 2)
                        .density(1f)
                        .friction(0f)
                        .restitution(0f)
                        .build())
		        .position(x, y)
		        .fixedRotation()
		        .type(BodyType.DynamicBody)
                .userData(this)
		        .build();
        body.setActive(false);

        onGroundSensor = new PlayerOnGroundSensor(0, 0, width / 2 * 0.9f, width / 2 * 0.2f, this, box2dworld);

        startPos = new Vector2(0, 0);
        lastVelocity = new Vector2(0, 0);
        sprite = new Sprite(new Texture(Gdx.files.internal("graphics/square-tile.png")));
        sprite.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        jumpSound = Gdx.audio.newSound(Gdx.files.internal("sounds/jump.wav"));
        explosionSound = Gdx.audio.newSound(Gdx.files.internal("sounds/explosion.wav"));
        finishSound = Gdx.audio.newSound(Gdx.files.internal("sounds/finish.wav"));
	}

	@Override
	public void update(float delta) {
        // Getting position
        setPosition(body.getWorldCenter().scl(Box2DWorld.BOX_TO_WORLD));
        lastVelocity.set(velocity);
        velocity.set(body.getLinearVelocity()).scl(Box2DWorld.BOX_TO_WORLD);

        onGroundSensor.update(delta);

        if(restartTime > 0) {
            restartTime -= delta;
            if(restartTime <= 0) {
                onGround = 0;
                body.setActive(true);
            }
            return;
        }

        // Die under screen
        if(y < - width || shouldRestart) {
            restartPos(true);
            explosionSound.play();
            shouldRestart = false;
            return;
        }

        if(body.getLinearVelocity().y > 0) {
            isGoingUp = true;
        } else {
            isGoingUp = false;
        }

        if(isGoingUp && !jumpButton) {
            body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y * 0.9f);
        }

        jumpingAnimation(delta);

        velocity.x = 3;

        if(onGround > 0 && jumpButton && firstJumpOnButtonPress) {
            body.applyLinearImpulse(new Vector2(0, 4f), body.getWorldCenter(), true);
            jumpSound.play();
            firstJumpOnButtonPress = false;
            isGoingUp = true;
            animationStarted = true;
        }

        float velChange = velocity.x - body.getLinearVelocity().x;
        float impulse = body.getMass() * velChange; //disregard time factor
        body.applyLinearImpulse(new Vector2(impulse, 0), body.getWorldCenter(), true);
	}

    public void jumpingAnimation(float delta) {

        if(isGoingUp && animationStarted) {

            if(sprite.getScaleY() < 1.5f && !maxSizeY) {
                sprite.setScale(sprite.getScaleX(), sprite.getScaleY() + 4 * delta);

                if(sprite.getScaleY() >= 1.5f) {
                    sprite.setScale(sprite.getScaleX(), 1.5f);
                    maxSizeY = true;
                }
            }

            if(sprite.getScaleX() > 0.6f && !maxSizeX) {
                sprite.setScale(sprite.getScaleX() - 4 * delta, sprite.getScaleY());

                if(sprite.getScaleX() <= 0.6f) {
                    sprite.setScale(0.6f, sprite.getScaleY());
                    maxSizeX = true;
                }
            }
        }

        if((!isGoingUp && animationStarted) || (maxSizeX && maxSizeY)) {


            if(sprite.getScaleY() > 1f) {
                sprite.setScale(sprite.getScaleX(), sprite.getScaleY() - 4 * delta);

                if(sprite.getScaleY() <= 1f) {
                    sprite.setScale(sprite.getScaleX(), 1f);
                }
            }

            if(sprite.getScaleX() < 1f) {
                sprite.setScale(sprite.getScaleX() + 4 * delta, sprite.getScaleY());

                if(sprite.getScaleX() >= 1f) {
                    sprite.setScale(1f, sprite.getScaleY());
                }
            }

            if(sprite.getScaleX() == 1 && sprite.getScaleY() == 1) {
                maxSizeX = false;
                maxSizeY = false;
                animationStarted = false;
            }
        }

    }


    @Override
	public void draw(SpriteBatch batch) {
//		sprite.draw(batch);
        sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
        sprite.draw(batch);
	}

    public void restartPos(boolean fastRestart) {
        body.setTransform(startPos.x * Box2DWorld.WORLD_TO_BOX, startPos.y * Box2DWorld.WORLD_TO_BOX, 0);
        body.setLinearVelocity(0, 0);
        body.setActive(false);
        onGround = 0;

        sprite.setScale(1, 1);
        maxSizeX = false;
        maxSizeY = false;
        animationStarted = false;
        isGoingUp = false;

        // Delay of setting onGround = 0 (allow box2d to end all contacts)
        if(fastRestart) {
            restartTime = 0.05f;
        } else {
            restartTime = 0.5f;
        }

    }

    @Override
    public void handleBeginContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {
//        if(psycho2 instanceof Ground || psycho2 instanceof  GhostPlayer) {
//            for(Vector2 contactPoint : contactPoints) {
//                if (Math.abs(body.getLocalPoint(contactPoint).x) < 0.32) {
//                    onGround = true;
//                    return;
//                }
//            }
//            return;
//        }

        if(psycho2 instanceof Spikes) {
            shouldRestart = true;
            map.explode(getPositionX(), getPositionY());
            return;
        }


        if(psycho2 instanceof Portal) {
            map.tweenFinish();
            finishSound.play();
            return;
        }
    }

    @Override
    public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {
//        if(psycho2 instanceof Ground || psycho2 instanceof  GhostPlayer) {
//            for(Vector2 contactPoint : contactPoints) {
//                if (Math.abs(body.getLocalPoint(contactPoint).x) < 0.32) {
//                    onGround = false;
//                    return;
//                }
//            }
//        }
    }

    public void setBodyPosition(Vector2 pos) {
        body.setTransform(pos.scl(Box2DWorld.WORLD_TO_BOX), 0);
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public boolean getFlagForDelete() {
        return false;
    }

    @Override
    public void setFlagForDelete(boolean flag) {

    }

    public void setStartPos(Vector2 startPos) {
        this.startPos.x = startPos.x;
        this.startPos.y = startPos.y;
    }

    public void setJumpButton(boolean jumpButton) {
        this.jumpButton = jumpButton;
        if(jumpButton) {
            firstJumpOnButtonPress = true;
        }
    }

    public void setColor(Color color) {
        sprite.setColor(color);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setRestartTime(float restartTime) {
        this.restartTime = restartTime;
    }

    @Override
    public void dispose() {

    }

    public class PlayerOnGroundSensor extends Entity implements PhysicsObject {

        private Body body;
        private Player player;

        public PlayerOnGroundSensor(float x, float y, float width, float height, Player player, Box2DWorld box2dworld) {
            super(x, y, width, height);

            this.player = player;

            this.body = box2dworld.getBodyBuilder()
                    .fixture(box2dworld.getFixtureDefBuilder()
                            .boxShape(width, height)
                            .density(1f)
                            .friction(0f)
                            .restitution(0f)
                            .sensor()
                            .build())
                    .position(x, y)
                    .fixedRotation()
                    .type(BodyType.DynamicBody)
                    .userData(this)
                    .build();
        }

        @Override
        public void draw(SpriteBatch batch) {

        }

        @Override
        public void update(float delta) {
            body.setLinearVelocity(0, 0);
            body.setTransform((player.getPositionX()) * Box2DWorld.WORLD_TO_BOX,
                    (player.getPositionY() - player.getWidth() / 2) * Box2DWorld.WORLD_TO_BOX, 0);
        }

        @Override
        public void dispose() {

        }

        @Override
        public void handleBeginContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {
            if(psycho2 instanceof Ground || psycho2 instanceof  GhostPlayer) {
                player.onGround++;
                return;
            }
        }

        @Override
        public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {
            if(psycho2 instanceof Ground || psycho2 instanceof  GhostPlayer) {
                player.onGround--;
                return;
            }
        }

        @Override
        public Body getBody() {
            return null;
        }

        @Override
        public boolean getFlagForDelete() {
            return false;
        }

        @Override
        public void setFlagForDelete(boolean flag) {

        }
    }
}
