package com.infunity.impossibleworlds.Model.Entities;

import com.badlogic.gdx.math.Vector2;

public abstract class MoveableEntity extends Entity {

	protected Vector2 velocity;
	protected float SPEED;
	protected float rotation;
	
	public MoveableEntity(float SPEED, float rotation, float width, float height, float x, float y){
		super(x, y, width, height);
		this.SPEED = SPEED;
		this.rotation = rotation;
		velocity = new Vector2(0, 0);
	}
	
	public Vector2 getVelocity(){
		return velocity;
	}
	
	public void setVelocity(Vector2 velocity){
		this.velocity = velocity;
	}
	
	public float getRotation(){
		return rotation;
	}
	
	public void setRotation(float rotation){
		this.rotation = rotation;
	}
	
	/**
	 * @return the sPEED
	 */
	public float getSPEED() {
		return SPEED;
	}

	/**
	 * @param sPEED the sPEED to set
	 */
	public void setSPEED(float sPEED) {
		SPEED = sPEED;
	}
	
}
