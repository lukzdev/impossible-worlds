package com.infunity.impossibleworlds.Model.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.infunity.impossibleworlds.Model.Box2DWorld;
import com.infunity.impossibleworlds.Model.Maps.Map;
import com.infunity.impossibleworlds.Model.PhysicsObject;
import com.infunity.impossibleworlds.Utils.ColorUtils;

/**
 * Created by Lukasz on 2014-08-23.
 */
public class Portal extends Entity implements PhysicsObject {

    private Body body;

    private Animation animation;
    private TextureRegion region;

    private float stateTime = 0;

    private Map map;

    public Portal(float x, float y, float width, float height, Map map, Box2DWorld box2dworld) {
        super(x, y, width, height);

        this.map = map;

        this.body = box2dworld.getBodyBuilder()
                .fixture(box2dworld.getFixtureDefBuilder()
                        .boxShape(width / 2, height / 2)
                        .density(1f)
                        .friction(0f)
                        .restitution(0f)
                        .sensor()
                        .build())
                .position(x + width / 2, y + height / 2)
                .fixedRotation()
                .type(BodyDef.BodyType.StaticBody)
                .userData(this)
                .build();

        TextureAtlas ta = new TextureAtlas(Gdx.files.internal("graphics/game.pack"));
        animation = new Animation(0.08f, ta.findRegions("portal"));

        region = animation.getKeyFrame(stateTime);
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.setColor(ColorUtils.layerToDarkColor(map.getCurrentLayer()));
        batch.draw(region, x, y);
        batch.setColor(Color.WHITE);
    }

    @Override
    public void update(float delta) {
        stateTime += delta;
        region = animation.getKeyFrame(stateTime, true);
    }

    @Override
    public void handleBeginContact(PhysicsObject psycho2,  Vector2[] contactPoints, Map map) {

    }

    @Override
    public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {

    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public boolean getFlagForDelete() {
        return false;
    }

    @Override
    public void setFlagForDelete(boolean flag) {

    }

    @Override
    public void dispose() {

    }
}
