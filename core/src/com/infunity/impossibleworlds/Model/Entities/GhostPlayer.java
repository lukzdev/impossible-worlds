package com.infunity.impossibleworlds.Model.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.infunity.impossibleworlds.Model.Box2DWorld;
import com.infunity.impossibleworlds.Model.LayerEnum;
import com.infunity.impossibleworlds.Model.Maps.Map;
import com.infunity.impossibleworlds.Model.PhysicsObject;
import com.infunity.impossibleworlds.Utils.ColorUtils;

/**
 * Created by Lukasz on 2014-08-23.
 */
public class GhostPlayer extends Entity implements PhysicsObject {

    private LayerEnum layer;
    private Body body;
    private Sprite sprite;

    private boolean isActive = false;

    public GhostPlayer(float x, float y, float width, float height, Box2DWorld box2dworld, LayerEnum layer) {
        super(x, y, width, height);
        this.layer = layer;

        // Player body
        this.body = box2dworld.getBodyBuilder()
                .fixture(box2dworld.getFixtureDefBuilder()
                        .boxShape(width / 2, width / 2)
                        .density(1f)
                        .friction(0f)
                        .restitution(0f)
                        .build())
                .position(x, y)
                .fixedRotation()
                .type(BodyDef.BodyType.StaticBody)
                .userData(this)
                .build();

        isActive = false;
        body.setActive(isActive);

        sprite = new Sprite(new Texture(Gdx.files.internal("graphics/square-tile.png")));
        sprite.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);

        sprite.setColor(ColorUtils.layerToDarkColor(layer));
    }

    @Override
    public void draw(SpriteBatch batch) {
        if(isActive) {
            sprite.draw(batch);
        }
    }

    @Override
    public void update(float delta) {

    }

    public void activate(float x, float y) {
        isActive = true;
        body.setActive(isActive);

        setPosition(x, y);
        sprite.setAlpha(1);
        sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
        body.setTransform(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 0);
    }

    public void deactivate() {
        isActive = false;
        body.setActive(isActive);
    }

    @Override
    public void handleBeginContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {

    }

    @Override
    public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {

    }

    public Sprite getSprite() {
        return sprite;
    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public boolean getFlagForDelete() {
        return false;
    }

    @Override
    public void setFlagForDelete(boolean flag) {

    }

    public LayerEnum getLayer() {
        return layer;
    }

    @Override
    public void dispose() {

    }
}
