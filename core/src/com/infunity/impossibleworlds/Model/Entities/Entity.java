package com.infunity.impossibleworlds.Model.Entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity {
	protected float x;
	protected float y;
    public float z = 0;
	protected float width;
	protected float height;
	protected Rectangle bounds;
	
	public Entity(float x, float y, float width, float height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		bounds = new Rectangle(x, y, width, height);
	}
	
	public abstract void draw(SpriteBatch batch);
    public abstract void update(float delta);
	public abstract void dispose();
	
	/**
	 * @return the position
	 */
	public float getPositionX() {
		return x;
	}
	
	public float getPositionY() {
		return y;
	}
	
	/**
	 * @param position the position to set
	 */
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}

    public void setPosition(Vector2 pos) {
        this.x = pos.x;
        this.y = pos.y;
    }
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @param width the width to set
	 */
	
	public void setWidth(float width) {
		this.width = width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * @param height the height to set
	 */
	
	public void setHeight(float height) {
		this.height = height;
	}
	
	/**
	 * @return the bounds
	 */
	public Rectangle getBounds() {
		return bounds;
	}
	
	/**
	 * @param bounds the bounds to set
	 */
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	
}
