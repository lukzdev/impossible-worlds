package com.infunity.impossibleworlds.Model.Entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.infunity.impossibleworlds.Model.Maps.Map;
import com.infunity.impossibleworlds.Model.PhysicsObject;

/**
 * Created by Lukasz on 2014-08-23.
 */
public class Spikes extends Entity implements PhysicsObject {

    private Body body;

    public Spikes(float x, float y, float width, float height, Body body) {
        super(x, y, width, height);
        this.body = body;
        body.setUserData(this);
    }

    @Override
    public void draw(SpriteBatch batch) {

    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void handleBeginContact(PhysicsObject psycho2,  Vector2[] contactPoints, Map map) {

    }

    @Override
    public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {

    }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public boolean getFlagForDelete() {
        return false;
    }

    @Override
    public void setFlagForDelete(boolean flag) {

    }

    @Override
    public void dispose() {

    }
}
